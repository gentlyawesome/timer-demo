## Introduction ##
This is a sample project using AngularJS and Material Design. The purpose of this project is to experiment an make a functional timer system where we can trace employee's number of hours.

## Installation ##
1. Install [Bower](https://bower.io/)
2. Install [NodeJS](https://nodejs.org/en/download/)
3. Install [NPM](https://docs.npmjs.com/getting-started/installing-node)
4. type **npm install serve -g** on your terminal or command prompt
5. then install dependencies using **bower install** 
6. then to start the server type **serve -p 12345**
7. finally, you can open up firefox or google chrome and type in http://localhost:12345