var mainApp = angular.module('timerApp', [
    'ngMaterial',
    'ngRoute',
    'timer',
    'firebase',
    'angular.filter',
    'ngAnimate'
  ]);
