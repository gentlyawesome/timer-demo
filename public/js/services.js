mainApp.service('employeeService', function($filter) {
    var self = this;

    self.employees = [
      // Format: 
      // {employee_id: "", position: "", name: "", img_src: "empty_avatar.jpg"},
      {employee_id: "123123123", position: "Head", name: "Jenelyne", img_src: "empty_avatar.gif"}
    ];
 
    self.getEmployee = function(employeeName) {
      var result = $filter('filter')(self.employees, { name: employeeName })[0];
      return result; 
    }; 

});
